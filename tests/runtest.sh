#!/bin/bash
. /usr/share/beakerlib/beakerlib.sh || exit 1

NAME=ibus-hangul

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm ${NAME}
        rlAssertRpm gnome-desktop-testing
        rlAssertRpm gnome-shell-extension-no-overview
        rlAssertBinaryOrigin gnome-desktop-testing-runner gnome-desktop-testing
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "pushd $tmp"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "ibus-desktop-testing-runner \
          --no-graphics \
          --runner=gnome \
          --timeout=1500 \
          --tests=${NAME} \
          --output=${NAME}.log \
          --result=test.log \
          " 0 "Running ${NAME} tests"
        echo "==== ${NAME}.log: ===="
        cat ${NAME}.log
        echo "==== EOF ${NAME}.log: ===="
        echo "==== test.log ===="
        cat test.log
        echo "==== EOF test.log: ===="
        rlAssertNotGrep FAIL test.log
        rlFileSubmit ${NAME}.log
        rlFileSubmit test.log
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
